var swiper = new Swiper(".swiper-container", {
  // 0 ~ 480px
  slidesPerView: 1.1,
  spaceBetween: 30,

  centeredSlides: true,
  grabCursor: true,
  loop: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  breakpoints: {
    // 480px ~ 768
    480: {
      slidesPerView: 2.5,
      spaceBetween: 30,
    },
    // 768px ~ 1190px
    768: {
      slidesPerView: 3.2,
      spaceBetween: 30,
    },
    // 1190px ~
    1190: {
      slidesPerView: 4,
      spaceBetween: 30,
    },
  },
});
